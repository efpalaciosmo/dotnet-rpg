﻿using AutoMapper;
using dotnet_rpg.DTOs.Character;
using Microsoft.AspNetCore.Http.HttpResults;

namespace dotnet_rpg;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Character, GetCharacterDto>();
        CreateMap<GetCharacterDto, Character>();
        CreateMap<CreateCharacterDto, Character>();
    }
}