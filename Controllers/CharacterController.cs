using dotnet_rpg.DTOs.Character;
using dotnet_rpg.Services.CharacterService;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_rpg.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _icCharacterService;

        public CharacterController(ICharacterService iCharacterService)
        {
            _icCharacterService = iCharacterService;
        }

        [HttpGetAttribute("characters")]
        public async Task<ActionResult<List<GetCharacterDto>>> GetCharacters()
        {
            return Ok(await _icCharacterService.GetCharacters());
        }

        [HttpGet("character/{id}")]
        public async Task<ActionResult<GetCharacterDto>> GetCharacter(int id)
        {
            return Ok(await _icCharacterService.GetCharacter(id));
        }

        [HttpPost("characters")]
        public async Task<ActionResult<List<GetCharacterDto>>> CreateCharacter(CreateCharacterDto createCharacterDto)
        {
            return Ok(await _icCharacterService.CreateCharacter(createCharacterDto));
        }

        [HttpPut("character")]
        public async Task<ActionResult<GetCharacterDto>> UpdateCharacter(UpdateCharacterDto updateCharacterDto)
        {
            var response = await _icCharacterService.UpdateCharacter(updateCharacterDto);
            if (response.Data is null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("character/{id}")]
        public async Task<ActionResult<GetCharacterDto>> DeleteCharacter(int id)
        {
            var response = await _icCharacterService.DeleteCharacter(id);
            if (response.Data is null)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
