﻿using dotnet_rpg.DTOs.Character;

namespace dotnet_rpg.Services.CharacterService;

public interface ICharacterService
{
    Task<ServiceResponse<List<GetCharacterDto>>> GetCharacters();
    Task<ServiceResponse<GetCharacterDto>> GetCharacter(int id);
    Task<ServiceResponse<List<GetCharacterDto>>> CreateCharacter(CreateCharacterDto createCharacterDto);
    Task<ServiceResponse<GetCharacterDto>> UpdateCharacter(UpdateCharacterDto updateCharacterDto);
    Task<ServiceResponse<GetCharacterDto>> DeleteCharacter(int id);
}