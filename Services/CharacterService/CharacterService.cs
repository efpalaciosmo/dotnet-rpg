﻿using AutoMapper;
using dotnet_rpg.DTOs.Character;
using Microsoft.AspNetCore.Http.HttpResults;

namespace dotnet_rpg.Services.CharacterService;

public class CharacterService: ICharacterService
{
    private static List<Character> _characters = new List<Character>
    {
        new Character(),
        new Character { Id = 1, Name = "efra" }
    };

    private readonly IMapper _mapper;
    public CharacterService(IMapper mapper)
    {
        this._mapper = mapper;
    }
    
    public async Task<ServiceResponse<List<GetCharacterDto>>> GetCharacters()
    {
        var serviceResponse = new ServiceResponse<List<GetCharacterDto>>();
        serviceResponse.Data = _characters.Select(c => _mapper.Map<GetCharacterDto>(c)).ToList();
        return serviceResponse;
    }

    public async Task<ServiceResponse<GetCharacterDto>> GetCharacter(int id)
    {
        var serviceResponse = new ServiceResponse<GetCharacterDto>();
        var character = _characters.FirstOrDefault(c => c.Id == id);
        serviceResponse.Data = _mapper.Map<GetCharacterDto>(character);
        if (character is not null)
        {
            return serviceResponse;
        }

        throw new Exception("Character not found");
    }

    public async Task<ServiceResponse<List<GetCharacterDto>>> CreateCharacter(CreateCharacterDto newCharacter)
    {
        var serviceResponse = new ServiceResponse<List<GetCharacterDto>>();
        var character = _mapper.Map<Character>(newCharacter);
        character.Id = _characters.Max(c => c.Id) + 1;
        _characters.Add(character);
        serviceResponse.Data = _characters.Select(c => _mapper.Map<GetCharacterDto>(c)).ToList();
        return serviceResponse;
    }

    public async Task<ServiceResponse<GetCharacterDto>> UpdateCharacter(UpdateCharacterDto updateCharacterDto)
    {
        var serviceResponse = new ServiceResponse<GetCharacterDto>();
        try
        {
            var character = _characters.FirstOrDefault(c => c.Id == updateCharacterDto.Id);
            if (character is null)
            {
                throw new Exception($"Character with Id '{updateCharacterDto.Id}' not found.");
            }
            _mapper.Map(updateCharacterDto, character);
            serviceResponse.Data = _mapper.Map<GetCharacterDto>(character);
        }
        catch (Exception e)
        {
            serviceResponse.Success = false;
            serviceResponse.Message = e.Message;
        }
        return serviceResponse;
    }

    public async Task<ServiceResponse<GetCharacterDto>> DeleteCharacter(int id)
    {
        var serviceResponse = new ServiceResponse<GetCharacterDto>();
        try
        {
            var character = _characters.FirstOrDefault(c => c.Id == id);
            if (character is null)
            {
                throw new Exception($"Character with id {id} didnt exists");
            }
            _characters.Remove(character);
            serviceResponse.Data = _mapper.Map<GetCharacterDto>(character);
        }
        catch (Exception e)
        {
            serviceResponse.Success = false;
            serviceResponse.Message = e.Message;
        }
        return serviceResponse;
    }
}